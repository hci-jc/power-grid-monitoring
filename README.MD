# Power Grid Anomaly Detection

<div align = "center">
   <img src = "/images/charlotte-venema-power-grid-unsplash.jpg"/>
</div>

## Objective
- Use probabilistic modeling (Hidden Markov Model) to detect threat intrusions in the power grid system.

## Technical Description
- Exploratory Data Analysis
- Data Cleaning and Imputation
- Dimension Reduction & Feature Selection via PCA
- Training Univariate Hidden Markov Model for Anomaly Detection
- Training Multivariate Hidden Markov Model for Anomaly Detection


## R files

<ul>
<li>remove_na.R
<ul>
<li>Removes all NA values from TermProjectData.txt and generates a new dataset TermProjectDataCleaned without NA values</li>
</ul>
</li>
<li>pca_plots_and_time_windows.R
<ul>
<li>Generates the PCA biplots and PCA summary tables for all 42 distinct time periods (Appendix A and Appendix B in the report)</li>
</ul>
</li>
<li>corr_matrix_and_univariate_hmm.R
<ul>
<li>Verifies the chosen time window from pca_plots_and_time_windows.R and generates a correlation heatmap (Variable Selection in the report)</li>
<li>Creates, trains, and tests the univariate HMM and outputs its results (Univariate Hidden Markov Model and Anomaly Detection in the report)</li>
</ul>
</li>
<li>multivariate_hmm.R
<ul>
<li>Creates, trains, and tests the multivariate HMM and outputs its results (Multivariate Hidden Markov Model and Anomaly Detection in the report)</li>
</ul>
</li>
<li>multivariate_hmm_alt.R
<ul>
<li>Creates, trains, and tests the multivariate HMM but with a different time window</li>
<li>Outputs the results of the multivariate HMM with different time window (Slide "Anomaly Detection With Multivariate HMM and Different Time Periods" in presentation)</li>
</ul>
</li>
</ul>
